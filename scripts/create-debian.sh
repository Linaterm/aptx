#!/bin/bash
mkdir -p build/debian
mkdir -p results
exec > build/debian.log 2>&1
mkdir -p build/debian/usr/bin
cp -rf aptx build/debian/usr/bin/aptx
chmod +x build/debian/usr/bin/aptx
mkdir -p build/debian/DEBIAN/
cp -rf control build/debian/DEBIAN/control
chmod 755 build/debian/DEBIAN
cd build
dpkg-deb --build debian
cd ..
mv build/debian.deb results
