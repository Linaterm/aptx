#!/bin/bash
mkdir -p build/termux
mkdir -p results
exec > build/termux.log 2>&1
mkdir -p build/termux/data/data/com.termux/files/usr/bin
cp -rf aptx build/termux/data/data/com.termux/files/usr/bin/aptx
chmod +x build/termux/data/data/com.termux/files/usr/bin/aptx
mkdir -p build/termux/DEBIAN/
cp -rf control build/termux/DEBIAN/control
chmod 755 build/termux/DEBIAN
cd build
dpkg-deb --build termux
cd ..
mv build/termux.deb results
