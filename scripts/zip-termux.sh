#!/bin/bash

cp -rf build/termux.log results/termux.log
apt-get update -qq && apt-get install zip -y
zip results/termux.zip results/termux*
rm results/termux.log results/termux.deb
