#!/bin/bash

cp -rf build/debian.log results/debian.log
apt-get update -qq && apt-get install zip -y
zip results/debian.zip results/debian*
rm results/debian.log results/debian.deb
